<?php
namespace EG\Sample\Model\Resource;

use EG\Sample\Api\Data\ExampleInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Example
 * @package EG\Sample\Model\ResourceModel
 */
class Example extends AbstractDb
{
    /**
     * @var string
     */
    const TABLE_NAME = 'sample_table';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(self::TABLE_NAME, ExampleInterface::ID);
    }
}