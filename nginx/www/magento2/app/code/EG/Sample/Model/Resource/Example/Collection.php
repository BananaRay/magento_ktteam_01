<?php
namespace EG\Sample\Model\Resource\Example;

use EG\Sample\Model\Example as Example;
use EG\Sample\Model\Resource\Example as ExampleResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package EG\Sample\Model\ResourceModel\Example
 */
class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(Example::class, ExampleResource::class);
    }
}
