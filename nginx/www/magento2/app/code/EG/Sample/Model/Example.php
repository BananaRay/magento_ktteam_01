<?php
namespace EG\Sample\Model;

use EG\Sample\Api\Data\ExampleInterface;
use EG\Sample\Model\Resource\Example as PostResource;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Example
 * @package EG\Sample\Model
 */
class Example extends AbstractModel implements ExampleInterface
{
    /**
     * @var string
     */
    protected $_idFieldName = ExampleInterface::ID; //@codingStandardsIgnoreLine

    /**
     * @inheritdoc
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(PostResource::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(ExampleInterface::ID);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setData(ExampleInterface::ID, $id);
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData(ExampleInterface::NAME);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->setData(ExampleInterface::NAME, $name);
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(ExampleInterface::DESCRIPTION);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->setData(ExampleInterface::DESCRIPTION, $description);
        return $this;
    }
    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(ExampleInterface::STATUS);
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status)
    {
        $this->setData(ExampleInterface::STATUS, $status);
        return $this;
    }



}
