<?php
namespace EG\Sample\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
          
          $data = [
              ['name' => 'Hydrogenium', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Lithium', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Carboneum', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Nitrogenium', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Oxygenium', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Magnesium', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Aluminium', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Sulfur', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Chlorum', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Hydrogenium1', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Lithium1', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Carboneum1', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Nitrogenium1', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Oxygenium1', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Magnesium1', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Aluminium1', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Sulfur1', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Chlorum1', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Hydrogenium2', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Lithium2', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Carboneum2', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Nitrogenium2', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Oxygenium2', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Magnesium2', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Aluminium2', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Sulfur2', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Chlorum2', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Hydrogenium3', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Lithium3', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Carboneum3', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Nitrogenium3', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Oxygenium3', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Magnesium3', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Aluminium3', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Sulfur3', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Chlorum3', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Hydrogenium4', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Lithium4', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Carboneum4', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Nitrogenium4', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Oxygenium4', 'description' => 'nonmetal', 'status' => 'gas'],
              ['name' => 'Magnesium4', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Aluminium4', 'description' => 'metal', 'status' => 'solid'],
              ['name' => 'Sulfur4', 'description' => 'nonmetal', 'status' => 'solid'],
              ['name' => 'Chlorum4', 'description' => 'nonmetal', 'status' => 'gas']


              
          ];
          foreach ($data as $bind) {
              $setup->getConnection()
                ->insertForce($setup->getTable('sample_table'), $bind);
          }
    }
}