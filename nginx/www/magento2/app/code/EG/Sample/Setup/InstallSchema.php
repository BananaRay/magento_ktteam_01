<?php
namespace EG\Sample\Setup;

use EG\Sample\Api\Data\ExampleInterface;
use EG\Sample\Model\Resource\Example as PostResource;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Security\Setup\InstallSchema as SecurityInstallSchema;
/**
 * Class InstallSchema
 * @package AlexPoletaev\Blog\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
          $setup->startSetup();

          $table = $setup->getConnection()
              ->newTable(
                  $setup->getTable(PostResource::TABLE_NAME)
              )
              ->addColumn(
                  ExampleInterface::ID,
                  Table::TYPE_INTEGER,
                  null,
                  ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                  'Element ID'
              )
              ->addColumn(
                  ExampleInterface::NAME,
                  Table::TYPE_TEXT,
                  255,
                  ['nullable' => false, 'default' => ''],
                    'Name'
              )
              ->addColumn(
                  ExampleInterface::DESCRIPTION,
                  Table::TYPE_TEXT,
                  255,
                  ['nullable' => false, 'default' => ''],
                    'Description'
              )
              ->addColumn(
                  ExampleInterface::STATUS,
                  Table::TYPE_TEXT,
                  255,
                  ['nullable' => false, 'default' => ''],
                    'status'
              )->setComment("Sample table");
          $setup->getConnection()->createTable($table);
          $setup->endSetup();
      }
}