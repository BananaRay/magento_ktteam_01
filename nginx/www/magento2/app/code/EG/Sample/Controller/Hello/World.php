<?php
namespace EG\Sample\Controller\Hello;
use Magento\Framework\App\Action\Context;

class World extends \Magento\Framework\App\Action\Action {
    protected $_resultExampleFactory;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultExampleFactory){
        $this->_resultExampleFactory = $resultExampleFactory;
        parent::__construct($context);
    }
    public function execute(){
        $resultExample = $this->_resultExampleFactory->create();
        $resultExample->getConfig()->getTitle()->set((__('HELLO WORLD')));
        return $resultExample;
    }
}
?>