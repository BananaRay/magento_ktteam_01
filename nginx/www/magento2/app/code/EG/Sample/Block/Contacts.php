<?php

namespace EG\Sample\Block;


use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;


class Contacts extends Template {

    protected $_contactFactory;
    public $testvar = 23;
    public function __construct(
        Context $context,
        \EG\Sample\Model\ExampleFactory $contactFactory,
        array $data = []
    ){
        $this->_contactFactory = $contactFactory;
        $this->setPageTitle("HELLO WORLD");
        parent::__construct($context, $data);
    }

    public function getContacts() {

        #$model = $this->_contactFactory->create();
        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 20;

        #$collection =  $model->getCollection();
        $collection = $this->_contactFactory->create()->getCollection();
        #$collection->addFieldToFilter('is_active', 1);
        #$collection->setOrder('id','asc');
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);


        return $collection;

    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ($this->getContacts()) {
            $pager =$this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'test.contacts.pager'
            )->setAvailableLimit(array(20=>20,50=>50))->setShowPerPage(true)->setCollection(
                    $this->getContacts()
                );
            $this->setChild('pager', $pager);
            $this->getContacts()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
?>