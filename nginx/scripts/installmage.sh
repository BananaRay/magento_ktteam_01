cd /var/www/html

wget https://github.com/magento/magento2/archive/2.0.tar.gz 
tar -xzvf 2.0.tar.gz 
mv magento2-2.0/ magento2/ 

cd /var/www/html/magento2
find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \;
find var vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} \;
chown -R :www-data .
chmod u+x bin/magento

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer

cd /var/www/html/magento2

composer install -v